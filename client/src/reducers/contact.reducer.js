import {
    CONTACT_FAIL,
    CONTACT_SUCCESS,
  } from "../actions/constants";
  
  const initialState = {
    // isSubmitted: null,
    loading: false,
    allcontact: [],
    customApp: {},
    getInTouch: {}
  };
  
  export default function(state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
      case CONTACT_SUCCESS:
        return {
          ...state,
          ...payload,
          loading: false
        };
      case CONTACT_FAIL:
        return {
          ...state,
          // isSubmitted: false,
          loading: false
        };
      default:
        return state;
    }
  }
  