import React, { Fragment, useState } from "react";
import { Icon, Card, Button, Typography, Row, Col, List, Spin, Input, Form, Anchor } from "antd";
import "antd/dist/antd.css";
import "../../assets/css/base/home.css";
import "../../assets/css/base/custom.css";
import "../../assets/css/base/main.css";
import "../../assets/css/base/homeNew.css";

// this page css
import "../../assets/css/home.css";

import { Route } from "react-router-dom";
import CountUp from 'react-countup'
import ReactPlayer from "react-player";
import { HashLink as Link } from 'react-router-hash-link';
import { NavLink } from "react-router-dom";
import moment from 'moment';
import "slick-carousel/slick/slick.css";

import img1 from '../../assets/img/img1.png'
import img2 from '../../assets/img/img2.png'

const HomeMain = () => {
  return (
    <React.Fragment>
      <div className="homeMain">
        {/* ===================
                    Start Landing  
                ===================  */}
        <Row type="flex" justify="space-around" align="middle" className="outermain" gutter={10}>
          <Col sm={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 12, order: 1 }} className="innermain">
            <a href="https://devfactory.upappfactory.com/" target="_blank">
            <div className="blockInner">
              <div className="blockInnerLeft">
                <h1>factory</h1>
                <div className="blockimgOuter">
                  <img src={img1} />
                </div>
                <p>Seamless experience, to your new eCommerce brand !</p>
              </div>
            </div>
            </a>
           
          </Col>
          <Col sm={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 12, order: 2 }} className="innermain">
          <a href="https://studio.upappfactory.com/" target="_blank">
            <div className="blockInner">
              <div className="blockInnerright">
                <h1>studio</h1>
                <div className="blockimgOuter">
                  <img src={img2} />
                </div>
                <p>Get your custom made website for your business ready !</p>
              </div>
            </div>
          </a>
          </Col>
        </Row>

        {/* ===================
                    End Landing  
                ===================  */}
      </div>
    </React.Fragment>
  );
};

export default HomeMain;
