/* eslint react/prop-types: 0 */
import React, { Fragment, useState, useEffect } from "react";
import { HashLink as NavLink } from 'react-router-hash-link';
import { connect } from "react-redux";
import { Layout, Menu, Form, Drawer, Button, Icon, Switch } from "antd";
import "../../assets/css/base/homeNew.css";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
const { Header } = Layout;

// let filteredApps;
const HeaderComp = ({ direction }) => {

  const [headerLink, setHeaderLink] = useState({
    visible: false,
  });

  const { visible } = headerLink;

  const onClose = () => {
    setHeaderLink({
      visible: false
    });
  };

  const showDrawer = () => {
    setHeaderLink({
      visible: true
    });
  };


  const generalHeader = (
    <Fragment>
      <Header className="responsive-header-menu">
        <NavLink className="brand-logo" to="/">
          <div className="logo" />
        </NavLink>
        <Button type="" style={{ marginLeft: "50px", fontSize: "30px", border: "2px solid" }} onClick={showDrawer} >
          <Icon type="align-right" />
        </Button>
        <Drawer title="Menu" placement={direction === DIRECTIONS.LTR ? 'left' : 'right'}
          closable={false} onClose={onClose} visible={visible}>
          <Menu className="drawer-v-Menu" theme="light" mode="vertical" defaultSelectedKeys={["0"]} onSelect={onClose}>
            <Menu.Item key="dr-factory">
              <NavLink to="/factory" className="mobile-menu-name-data">About Us</NavLink>
            </Menu.Item>
            <Menu.Item key="dr-pricing">
              <NavLink to="/pricing" className="mobile-menu-name-data">Clientele</NavLink>
            </Menu.Item>
            <Menu.Item key="dr-support">
              <NavLink to="/" className="mobile-menu-name-data">Consultant</NavLink>
            </Menu.Item>
            <Menu.Item key="dr-support">
              <NavLink to="/" className="mobile-menu-name-data">Pricing</NavLink>
            </Menu.Item>
            <Menu.Item key="dr-support">
              <NavLink to="/" className="mobile-menu-name-data">Contact Us</NavLink>
            </Menu.Item>
          </Menu>
        </Drawer>
      </Header>
      {direction === DIRECTIONS.LTR && <Fragment>
        <Header className="header-comp">
          <NavLink to="/">
            <div className="logo" />
          </NavLink>
          <Menu className="U-Menu" theme="light" mode="horizontal" defaultSelectedKeys={["0"]}>
            <Menu.Item key="n-factory">
              <NavLink activeClassName="is-active" to="#uaf_cp_id_aboutUs">About Us</NavLink>
            </Menu.Item>
            <Menu.Item >
              <NavLink to="#uaf_cp_id_whyUs">Clientele</NavLink>
            </Menu.Item>
            <Menu.Item >
              <NavLink to="#uaf_cp_id_consultant">Management</NavLink>
            </Menu.Item>
            <Menu.Item >
              <NavLink to="#uaf_cp_id_pricing">Blog</NavLink>
            </Menu.Item>
            <Menu.Item >
              <NavLink to="#uaf_cp_id_ContactUs">Contact Us</NavLink>
            </Menu.Item>
          </Menu>
        </Header>
      </Fragment>}
      {direction === DIRECTIONS.RTL && <Fragment>
        <Header className="header-comp float-right">
          <NavLink to="/">
            <div className="logo float-right" />
          </NavLink>
          <Menu className="U-Menu" theme="light" mode="horizontal" defaultSelectedKeys={["0"]}>
            <Menu.Item >
              <NavLink activeClassName="is-active" to="#uaf_cp_id_aboutUs">About Us</NavLink>
            </Menu.Item>
            <Menu.Item>
              <NavLink activeClassName="is-active" to="#uaf_cp_id_whyUs">Clientele</NavLink>
            </Menu.Item>
            <Menu.Item>
              <NavLink activeClassName="is-active" to="#uaf_cp_id_consultant">Management</NavLink>
            </Menu.Item>
            <Menu.Item>
              <NavLink activeClassName="is-active" to="#uaf_cp_id_pricing">Blog</NavLink>
            </Menu.Item>
            <Menu.Item key="n-factory">
              <NavLink activeClassName="is-active" to="#uaf_cp_id_ContactUs">Contact Us</NavLink>
            </Menu.Item>
          </Menu>
        </Header>
      </Fragment>}
    </Fragment>
  );

  return (
    <Fragment>
      {generalHeader}
    </Fragment>
  );
};

HeaderComp.prototypes = {
  direction: withDirectionPropTypes.direction,
};

export default connect(null, null)(withDirection(HeaderComp));
