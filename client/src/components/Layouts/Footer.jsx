import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Layout, Row, Col, Typography } from "antd";
import withDirection from 'react-with-direction';
import Logo from "../../assets/img/logo.png"
const { Title, Text } = Typography;
const { Footer } = Layout;

const FooterComp = () => {
  const generalFooter = (
    <Fragment>
      <Footer className="app-footer" style={{ paddingTop: "100px" }}>
        <Row gutter={16} type="flex" justify="center" align="top" style={{margin:"0"}}>
          <Col lg={5} md={4} sm={24} xs={24} style={{ textAlign: "center" }}>
            <Link to="/">
              <img
                src={Logo}
                alt="its a footer logo"
                style={{ width: "80px" }}
              />
            </Link>
          </Col>
          <Col lg={5} md={4} sm={24} xs={12}>
            <div className="footer-section">
              <div>
                <Title className="fontSize-20">Types</Title>
              </div>
              <div className="footer-nav-item">
                <Link to="/eCommerce-Single-Vendor-Platform">
                  <Text>eCommerce Apps</Text>
                </Link>
              </div>
              <div className="footer-nav-item">
                <Link to="/facility-management-application">
                  <Text>Facility Management Apps</Text>
                </Link>
              </div>
              <div className="footer-nav-item">
                <Link to="/restaurant-application">
                  <Text>Restaurant App</Text>
                </Link>
              </div>
              <div className="footer-nav-item">
                <Link to="/upappbot">
                  <Text>UPapp Bots</Text>
                </Link>
              </div>
            </div>
          </Col>
          <Col lg={5} md={6} sm={24} xs={12}>
            <div className="footer-section">
              <div>
                <Title className="fontSize-20">Top Apps</Title>
              </div>
              <div className="footer-nav-item">
                <a href="https://upappfactory.com/readymade-apps/online-fashion-app/UAF-EC-00010">
                  <Text>Fashion eCommerce App</Text>
                </a>
              </div>
              <div className="footer-nav-item">
                <a href="https://upappfactory.com/readymade-apps/online-jewellery-app/UAF-EC-00013">
                  <Text>Jewellery Shop</Text>
                </a>
              </div>
            </div>
          </Col>
          <Col lg={5} md={6} sm={24} xs={12}>
            <div className="footer-section">
              <div>
                <Title className="fontSize-20">Our Company</Title>
              </div>
              <div className="footer-nav-item">
                <Link to="/about-us">
                  <Text>About Us</Text>
                </Link>
              </div>
              <div className="footer-nav-item">
                <Link to="/terms-service">
                  <Text>Terms of Service</Text>
                </Link>
              </div>

              <div className="footer-nav-item">
                <a
                  target="_blank"
                  href="https://www.instagram.com/upappfactory/"
                >
                  <Text>Privacy Policy</Text>
                </a>
              </div>

              <div className="footer-nav-item">
                <a
                  target="_blank"
                  href="https://www.instagram.com/upappfactory/"
                >
                  <Text>Cancellation & Refund Policy</Text>
                </a>
              </div>
              <div className="footer-nav-item">
                <a
                  target="_blank"
                  href="https://www.instagram.com/upappfactory/"
                >
                  <Text>Contact Us</Text>
                </a>
              </div>
              <div className="footer-nav-item">
                <a
                  target="_blank"
                  href="https://www.instagram.com/upappfactory/"
                >
                  <Text>Clientele</Text>
                </a>
              </div>
              <div className="footer-nav-item">
                <a
                  target="_blank"
                  href="https://www.instagram.com/upappfactory/"
                >
                  <Text>Blogs</Text>
                </a>
              </div>
            </div>
          </Col>
          <Col lg={4} md={3} sm={24} xs={12}>
            <div className="footer-section">
              <div>
                <Title className="fontSize-20">Follow Us</Title>
              </div>
              <div className="footer-nav-item">
                <a
                  target="_blank"
                  href="https://www.instagram.com/upappfactory/"
                >
                  <Text>Instagram</Text>
                </a>
              </div>
              <div className="footer-nav-item">
                <a
                  target="_blank"
                  href="https://www.facebook.com/UpAppFactory/"
                >
                  <Text>Facebook</Text>
                </a>
              </div>

              <div className="footer-nav-item">
                <a
                  target="_blank"
                  href="https://www.linkedin.com/company/upappfactory/"
                >
                  <Text>Linkedin</Text>
                </a>
              </div>
            </div>
          </Col>
        </Row>
        <Row gutter={16} style={{ margin: 0, padding: "30px 0px 10px 0px" }}>
          <Col
            lg={24}
            md={24}
            sm={24}
            xs={24}
            style={{ padding: "5px", textAlign: "center" }}
            className="footer-copy-right"
          >
            <Link to="/">UPapp factory</Link> ©2020 all rights are reserved
          </Col>
        </Row>
      </Footer>
    </Fragment>
  );
  return (
    <Fragment>
      {generalFooter}
    </Fragment>
  );
};


export default connect(null, null)(
  withDirection(FooterComp)
);
