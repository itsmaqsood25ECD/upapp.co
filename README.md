# up-app-factory
## Dependencies to be installed locally

* nodejs
* mongodb
* sourcetree
* Any IDE

## How to clone from gitlab?

1. copy https clone URL
2. in sourcetree choose clone option
3. paste the path, choose the destination, give a empty folder name and clone.

## How to run?

1. cd to where mongodb is installed locally default path "cd Program\ Files/MongoDB/Server/4.0/bin and run "./mongod"
2. cd to cloned path and run cmd  "npm install yarn -g" then "yarn install"
3. run cmd "yarn run serve"

its running in localhost:3000