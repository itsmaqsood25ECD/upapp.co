const mongoose = require('mongoose')
const moment = require('moment')

const Schema = mongoose.Schema

const BlogSchema = new Schema({
  UID: {
    type: String,
    required: true,
    unique: true
  },
  title: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  featured_image: {
    type: String
  },
  slug: {
    type:String
  },
  author: {
    type: String
  },
  date_addedd: {
    type: String,
    default: moment().format()
  },
  date_modified: {
    type: String,
    default: moment().format()
  }
})

const Blogs = mongoose.model('Blogs', BlogSchema)
module.exports = Blogs