const mongoose = require('mongoose')
const validator = require('validator')

const Schema = mongoose.Schema

const CandidateSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error('please type in a valid email')
      }
    },
    required: true
  },
  resume: {
    url: {
      type: String,
      required: true
    }
  },
  created_at: {
    type: String,
    default: Date.now,
    required: true
  },
  job_title: {
    type: String
  },
  job_location: {
    type: String
  },
  UID: {
    type: String
  }
})

const Candidate = mongoose.model('Candidate', CandidateSchema)
module.exports = Candidate