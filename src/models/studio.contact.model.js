const mongoose = require('mongoose')
const validator = require('validator')
const moment = require('moment')

const Schema = mongoose.Schema


const StudiocontactSchema = new Schema({
	UID: {
		type: String,
		required: true
	},
	firstname: {
		type: String,
		required: true
	},
	lastname: {
		type: String,
		required: true
	},
	email: {
		type: String,
		lowercase: true,
		required:true,
		validate(value) {
			if (!validator.isEmail(value)) {
				throw new Error('please type in a valid email')
			}
		}
	},
	country: {
		type: String,
	},
	budget: {
		type: String,
	},
	description: {
		type: String,
	},
	hearedAt: {
		type: String,
	},
	createdAt: {
		type: String,
		default: moment().format(),
	},
	submittedAt: {
		type: String,
		default: Date.now(),
	},
	ticket_no: {
		type: Number,
		required: true,
		default: Date.now()

	},
	subject: {
		type: String
	},
	
})

const StudioContact = mongoose.model("StudioContact", StudiocontactSchema)
module.exports = StudioContact