const mongoose = require('mongoose')
const validator = require('validator')

const Schema = mongoose.Schema

const PartnersSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    lowercase: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error('please type in a valid email')
      }
    },
    required: true
  },
  mobile: {
    type: String,
    required: true
  },
  country: {
    type: String,
    required: true
  },
  company_size: {
    type: String,
    required: true
  },
  created_at: {
    type: String,
    default: Date.now()
  }
})

const Partners = mongoose.model("Partners", PartnersSchema)
module.exports = Partners