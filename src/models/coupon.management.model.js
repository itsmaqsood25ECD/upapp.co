const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const CouponSchema = new Schema({
    coupon_code: {
        type: String,
        required: true,
        unique: true
    },
    coupon_description: {
        type: String,
    },
    general: {
        discount_type: {
            type: String,
            required: true
        },
        coupon_amount: {
            type: Number,
            required: true
        },
        coupon_expiry_date: {
            type: Date,
            required: true
        },
        coupon_pack_type: {
            type: String,
            required: true
        }
    },
    usage_restriction: {
        min_spend: {
            type: Number
        },
        max_spend: {
            type: Number
        },
        products: [{
            type: String
        }],
        exclude_products: [{
            type: String
        }],
        include_category: [{
            type: String
        }],
        exclude_category: [{
            type: String
        }],
        allowed_emails: [{
            type: String
        }]
    },
    usage_limits: {
        usage_limit_per_coupon: {
            type: Number
        },
        usage_limit_x_items: {
            type: Number
        },
        usage_limit_per_user: {
            type: Number
        },
        usage_limit_per_coupon_used: {
            type: Number,
            default: 0
        }
    },
    createdAt: {
        type: Date,
        default: Date.now()
    }
})

// find user by credentials for login
CouponSchema.statics.findByName = async (coupon_code) => {
    const coupon = await Coupon.findOne({ coupon_code })
    return coupon
}

const Coupon = mongoose.model("Coupon", CouponSchema)
module.exports = Coupon