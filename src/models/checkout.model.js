const mongoose = require('mongoose')

const Schema = mongoose.Schema

const checkoutSchema = new Schema({
  delivery_mode: [{
    type: String
  }],
  prefered_payment_mode_for_purchase: [{
    type: String
  }],
  host_your_webiste_on: [{
    type: String
  }],
  promotional_preferences: [{
    type: String
  }],
  restriction_for_securing_delivery: [{
    type: String
  }],
  prefered_currency_type: [{
    type: String
  }],
  number_of_category: [{
    type: String
  }],
  looking_for: {
    type: String
  },
  domain_name: {
    type: String
  },
  tagline: {
    type: String
  },
  languages: {
    type: String
  },
  market_place: {
    type: String
  },
  front_page:{
    type: String
  },
  business_logo:{
    type: String
  },
  business_model: {
    type: String
  },
  primary_color: {
    type: String
  },
  secondary_color: {
    type: String
  },
  order_status_for_store: [{
    type: String
  }],
  columns_for_vendor_page: [{
    type: String
  }],
  vendor_catalog_setting_page: [{
    type: String
  }],
  order_limitations: [{
    type: String
  }],
  columns_for_order_info_page: [{
    type: String
  }],
  vendor_setting_page: [{
    type: String
  }],
  availability_for_vendor: [{
    type: String
  }],
  analytics_platform: [{
    type: String
  }],
  promotion_type: [{
    type: String
  }],
  mode_of_notification: [{
    type: String
  }],

})

const Checkout = mongoose.model('Checkout', checkoutSchema)
module.exports = Checkout