const mongoose = require('mongoose')
const moment = require('moment')
const validator = require('validator')


const Schema = mongoose.Schema

const EventsRequestSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    lowercase: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error('please type in a valid email')
      }
    }
  },
  mobile: {
    type: String,
    required: true
  },
  city: {
    type: String
  },
  country: {
    type: String,
    required: true
  },
  created_at: {
    type: String,
    default: moment().format()
  },
  description: {
    type: String,
    required: true
  },
  designation: {
    type: String,
    required: true
  }
})

const EventsRequest = mongoose.model("EventsRequest", EventsRequestSchema)
module.exports = EventsRequest