const mongoose = require('mongoose')
const validator = require('validator')

const Schema = mongoose.Schema;

const SubscriptionSchema = new Schema({
  UID: {
    type: String,
    require: true
  },
  email: {
    type: String,
    required: true,
    lowercase: true,
    unique: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error('please type in a valid email')
      }
    }
  },
  createdAt: {
    type: String,
    default: Date.now()
  }
})

const Subscription = mongoose.model("Subcription", SubscriptionSchema)
module.exports = Subscription