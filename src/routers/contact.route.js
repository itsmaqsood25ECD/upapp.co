const express = require('express')
const Contact = require('../models/contact.model')
const nodemailer = require('nodemailer')
const moment = require('moment')
const mysql = require('mysql');

let connection = mysql.createPool({
	host: '74.208.88.152',
	user: 'admin_crm',
	password: 'Admin@123',
	database: 'admin_crm'
});


const router = new express.Router()

// Send the email
const transporter = nodemailer.createTransport({
	host: 'smtp.zoho.com',
	port: 465,
	secure: true,
	auth: {
		user: process.env.EMAIL_USERNAME,
		pass: process.env.EMAIL_PASSWORD
	}
})
// Send the email
const personalTransporter = nodemailer.createTransport({
	host: 'smtp.zoho.com',
	port: 465,
	secure: true,
	auth: {
		user: 'harris@upappfactory.com',
		pass: 'Harris@1234^'
	}
})

// // Send the email
// const BusinessTransporter = nodemailer.createTransport({
// 	host: 'smtp.zoho.com',
// 	port: 465,
// 	secure: true,
// 	auth: {
// 		user: 'samual@upappfactory.com',
// 		pass: 'samual@1234'
// 	}
// })

const OmanTransporter = nodemailer.createTransport({
	host: 'smtp.zoho.com',
	port: 465,
	secure: true,
	auth: {
		user: 'zach@upappfactory.com',
		pass: 'UPapp^123'
	}
})



connection.getConnection(function (err) {
	if (err) {
		console.error('error connecting: ' + err.stack);
		// connection = reconnect(connection)
		return;
	}

	console.log('My sql connected ');
});

function reconnect(connection) {
	console.log("\n New connection tentative...");

	//- Destroy the current connection variable
	if (connection) connection.destroy();

	//- Create a new one
	connection = mysql.createPool({
		host: '74.208.88.152',
		user: 'admin_crm',
		password: 'Admin@123',
		database: 'admin_crm'
	});

	//- Try to reconnect
	connection.getConnection(function (err) {
		if (err) {
			//- Try to connect every 2 seconds.
			setTimeout(reconnect(connection), 2000);
		} else {
			console.log("\n\t *** New connection established with the database. ***")
			console.log('connected as id ' + connection.threadId);
			return connection;
		}
	});
}

//- Error listener
connection.on('error', function (err) {
	console.log(err, "error")
	//- The server close the connection.
	if (err.code === "PROTOCOL_CONNECTION_LOST") {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
		connection = reconnect(connection);
	}

	//- Connection in closing
	else if (err.code === "PROTOCOL_ENQUEUE_AFTER_QUIT") {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
		connection = reconnect(connection);
	}

	//- Fatal error : connection variable must be recreated
	else if (err.code === "PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR") {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
		connection = reconnect(connection);
	}

	//- Error because a connection is already being established
	else if (err.code === "PROTOCOL_ENQUEUE_HANDSHAKE_TWICE") {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
	}

	//- Anything else
	else {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
		connection = reconnect(connection);
	}

});

// post contact form data
router.post('/contact-us', async (req, res) => {
	try {
		const prevContact = await Contact.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
		let counter, UID, UAF
		if (prevContact.length === 0) {
			counter = 1
		} else {
			counter = prevContact[0].UID.slice(8)
			counter = +counter + 1
		}
		UAF = "UAF-CON-"
		if (counter < 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 100 && counter >= 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 1000 && counter >= 100) {
			UID = `${UAF}000000${counter}`
		} else if (counter < 10000 && counter >= 1000) {
			UID = `${UAF}00000${counter}`
		} else if (counter < 100000 && counter >= 10000) {
			UID = `${UAF}0000${counter}`
		} else if (counter < 1000000 && counter >= 100000) {
			UID = `${UAF}000${counter}`
		} else if (counter < 10000000 && counter >= 1000000) {
			UID = `${UAF}00${counter}`
		} else if (counter < 100000000 && counter >= 10000000) {
			UID = `${UAF}0${counter}`
		} else {
			UID = `${UAF}${counter}`
		}
		const contact = new Contact(req.body)
		contact.ticket_no = Date.now()
		contact.UID = UID
		if (!contact)
			return res.status(400).send({ error: true, success: false, message: "Bad Request", result: {} })
		await contact.save()
		let mailOptions, notify
		if (contact.type === 'ExistingUser' && contact.category === 'Billing') {
			mailOptions = {
				from: process.env.EMAIL_USERNAME,
				to: req.body.email,
				subject: `#${contact.UID} Call Back Request for ${contact.name}, ${moment().format('L')}`,
				bcc: "payments@upappfactory.com",
				html: `<!DOCTYPE html>
				<html lang="en">
					<head>
						<meta charset="UTF-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
						<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
						<title>
							Contact Us
						</title>
					</head>
	
					<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
						<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
							<tbody>
								<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
									<td style="padding-left: 15px;">
										<a href="">
											<img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
										</a>
									</td>
								</tr>
								<td style="font-size:14px; font-family: Helvetica, sans-serif; color: #333333;padding: 20px;">
									Dear ${contact.name},
									<br /><br />
									We understand that you are trying to reach out to the support team
									at UPappfactory.
									Since we have received your request with the ticket
									#${contact.UID}, our support team will be in touch with you
									very soon to assist you with your concerns.
									<br /><br />
									Regards,
									<br />
									Support Team
									<br />
									UPappfactory
								</td>
								<tr style="text-align: center;background: #f7f7f7">
									<td style="padding:0px 20px;text-align: center;background: #f7f7f7">
									</td>
								</tr>
								<tr style="text-align: center;background: #f7f7f7">
									<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
										<p style="margin: 10px auto 0px auto;line-height: 0.8;">
											<a href="mailto:contactus@upappfactory.com" style="text-decoration: none;">contactus@upappfactory.com</a>
											|
											<a style="text-decoration: none;" href="www.upappfactory.com">Upappfactory</a><br /><br />
										</p>
										<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
										<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
										<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
									</td>
								</tr>
								<tr style="padding:20px;text-align: center;background: #f7f7f7">
									<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
										Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
									</td>
								</tr>
							</tbody>
						</table>
					</body>
				</html>`
			}
			notify = {
				from: process.env.EMAIL_USERNAME,
				to: "payments@upappfactory.com",
				subject: `#${contact.UID} Call Back Request for ${contact.name}, ${moment().format('L')}`,
				bcc: "ali@upappfactory.com, aslam@upappfactory.com",
				html: `<!DOCTYPE html>
				<html lang="en">
	
				<head>
					<meta charset="UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					<title>Call Back Request</title>
					<style>
						.bank td,
						th {
							border: 1px solid #dddddd;
							text-align: left;
							padding: 8px;
						}
	
						.bank tr:nth-child(even) {
							background-color: #dddddd;
						}
					</style>
				</head>
	
				<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
					<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
						<tbody>
							<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
								<td style="
								 padding-left: 15px;
							">
									<a href="">
										<img style="width: 150px" width="150"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
									</a>
								</td>
	
							</tr>
							<tr>
								<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
									Hi Sales Team,
									<br>
									We have received a new Call Back request with ticket no: ${contact.UID}, please find the details below:
									<br>
									<br>
									<table class="bank" style="width:100%;border-spacing: 0px;border:none">
	
										<tr>
											<td>Ticket No</td>
											<td>${contact.UID}</td>
										</tr>
										<tr>
											<td>Name</td>
											<td>${contact.name}</td>
										</tr>
										<tr>
											<td>Email Address</td>
											<td>${contact.email}</td>
										</tr>
										<tr>
											<td>Phone No</td>
											<td>${contact.mobile}</td>
										</tr>
										<tr>
											<td>Message</td>
											<td>${contact.description}</td>
										</tr>
									</table>
	
									<br />
									Thanks!
									<br>
									<br />
									Regards,
									<br />
									UPapp factory
								</td>
							</tr>
							<tr style="text-align: center;background: #f7f7f7">
	
								<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
									<p style="margin: 10px auto 0px auto;line-height: 0.8;">
										<a href="mailto:contactus@upappfactory.com" style="text-decoration: none;">contactus@upappfactory.com</a>
										|
										<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
									</p>
									<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
									<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
									<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
								</td>
	
							</tr>
							<tr style="padding:20px;text-align: center;background: #f7f7f7">
								<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
									Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
								</td>
							</tr>
						</tbody>
					</table>
				</body>
				</html>`
			}
		} else if (contact.type === 'ExistingUser' && contact.category === 'technicalsupport') {
			mailOptions = {
				from: process.env.EMAIL_USERNAME,
				to: req.body.email,
				subject: `#${contact.UID} Call Back Request for ${contact.name}, ${moment().format('L')}`,
				bcc: "help@upappfactory.com",
				html: `<!DOCTYPE html>
				<html lang="en">
					<head>
						<meta charset="UTF-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
						<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
						<title>
							Contact Us
						</title>
					</head>
	
					<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
						<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
							<tbody>
								<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
									<td style="padding-left: 15px;">
										<a href="">
											<img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
										</a>
									</td>
								</tr>
								<td style="font-size:14px; font-family: Helvetica, sans-serif; color: #333333;padding: 20px;">
									Dear ${contact.name},
									<br /><br />
									We understand that you are trying to reach out to the support team
									at UPappfactory.
									Since we have received your request with the ticket
									#${contact.UID}, our support team will be in touch with you
									very soon to assist you with your concerns.
									<br /><br />
									Regards,
									<br />
									Support Team
									<br />
									UPappfactory
								</td>
								<tr style="text-align: center;background: #f7f7f7">
									<td style="padding:0px 20px;text-align: center;background: #f7f7f7">
									</td>
								</tr>
								<tr style="text-align: center;background: #f7f7f7">
									<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
										<p style="margin: 10px auto 0px auto;line-height: 0.8;">
											<a href="mailto:contactus@upappfactory.com" style="text-decoration: none;">contactus@upappfactory.com</a>
											|
											<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
										</p>
										<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
										<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
										<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
									</td>
								</tr>
								<tr style="padding:20px;text-align: center;background: #f7f7f7">
									<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
										Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
									</td>
								</tr>
							</tbody>
						</table>
					</body>
				</html>`
			}
			notify = {
				from: process.env.EMAIL_USERNAME,
				to: "help@upappfactory.com",
				subject: `#${contact.UID} Call Back Request for ${contact.name}, ${moment().format('L')}`,
				bcc: "ali@upappfactory.com, aslam@upappfactory.com",
				html: `<!DOCTYPE html>
				<html lang="en">
	
				<head>
					<meta charset="UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					<title>Call Back Request</title>
					<style>
						.bank td,
						th {
							border: 1px solid #dddddd;
							text-align: left;
							padding: 8px;
						}
	
						.bank tr:nth-child(even) {
							background-color: #dddddd;
						}
					</style>
				</head>
	
				<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
					<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
						<tbody>
							<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
								<td style="
								 padding-left: 15px;
							">
									<a href="">
										<img style="width: 150px" width="150"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
									</a>
								</td>
	
							</tr>
							<tr>
								<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
									Hi Sales Team,
									<br>
									We have received a new Call Back request with ticket no: ${contact.UID}, please find the details below:
									<br>
									<br>
									<table class="bank" style="width:100%;border-spacing: 0px;border:none">
	
										<tr>
											<td>Ticket No</td>
											<td>${contact.UID}</td>
										</tr>
										<tr>
											<td>Name</td>
											<td>${contact.name}</td>
										</tr>
										<tr>
											<td>Email Address</td>
											<td>${contact.email}</td>
										</tr>
										<tr>
											<td>Phone No</td>
											<td>${contact.mobile}</td>
										</tr>
										<tr>
											<td>Message</td>
											<td>${contact.description}</td>
										</tr>
									</table>
	
									<br />
									Thanks!
									<br>
									<br />
									Regards,
									<br />
									UPapp factory
								</td>
							</tr>
							<tr style="text-align: center;background: #f7f7f7">
	
								<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
									<p style="margin: 10px auto 0px auto;line-height: 0.8;">
										<a href="mailto:contactus@upappfactory.com" style="text-decoration: none;">contactus@upappfactory.com</a>
										|
										<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
									</p>
									<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
									<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
									<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
								</td>
	
							</tr>
							<tr style="padding:20px;text-align: center;background: #f7f7f7">
								<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
									Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
								</td>
							</tr>
						</tbody>
					</table>
				</body>
				</html>`
			}
		} else {

			const message1 = `\n*****	New Customer *****\n Message: ${contact.description}\n`
			//  name,email,country,mobile,interseted_in,UID 
			// name,source,stage_id,mobile,email,country,sales_rep,lead_score,due_date,next_followup,unsubcribed_at,token,created_at,updated_at,rating_status

			connection.query("INSERT INTO `fx_leads` (`name`,`email`,`country`,`mobile`,`token`,`stage_id`,`sales_rep`,`lead_score`,`due_date`,`next_followup`,`unsubscribed_at`,`created_at`,`updated_at`,`rating_status`,`source`,`message`) VALUES ('" + contact.name + "','" + contact.email + "','" + contact.country + "','" + contact.mobile + "','" + UID + "','42','18','10',DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),'cold','51','" + contact.description + "') ON DUPLICATE KEY UPDATE `message`  = (SELECT CONCAT(message,'" + message1 + "') as msg FROM fx_leads as leads WHERE email='" + contact.email + "')")

			mailOptions = {
				from: process.env.EMAIL_USERNAME,
				to: req.body.email,
				subject: `#${contact.UID} Call Back Request for ${contact.name}, ${moment().format('L')}`,
				bcc: "contactus@upappfactory.com, sales@upappfactory.com",
				html: `<!DOCTYPE html>
					<html lang="en">
						<head>
							<meta charset="UTF-8" />
							<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
							<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
							<title>
								Contact Us
							</title>
						</head>
		
						<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
							<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
								<tbody>
									<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
										<td style="padding-left: 15px;">
											<a href="">
												<img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
											</a>
										</td>
									</tr>
									<td style="font-size:14px; font-family: Helvetica, sans-serif; color: #333333;padding: 20px;">
										Dear ${contact.name},
										<br /><br />
										We understand that you are trying to reach out to the support team
										at UPappfactory.
										Since we have received your request with the ticket
										#${contact.UID}, our support team will be in touch with you
										very soon to assist you with your concerns.
										<br /><br />
										Regards,
										<br />
										Support Team
										<br />
										UPappfactory
									</td>
									<tr style="text-align: center;background: #f7f7f7">
										<td style="padding:0px 20px;text-align: center;background: #f7f7f7">
										</td>
									</tr>
									<tr style="text-align: center;background: #f7f7f7">
										<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
											<p style="margin: 10px auto 0px auto;line-height: 0.8;">
												<a href="mailto:contactus@upappfactory.com" style="text-decoration: none;">contactus@upappfactory.com</a>
												|
												<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
											</p>
											<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
											<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
											<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
										</td>
									</tr>
									<tr style="padding:20px;text-align: center;background: #f7f7f7">
										<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
											Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
										</td>
									</tr>
								</tbody>
							</table>
						</body>
					</html>`
			}
			notify = {
				from: process.env.EMAIL_USERNAME,
				to: "contactus@upappfactory.com",
				subject: `#${contact.UID} Call Back Request for ${contact.name}, ${moment().format('L')}`,
				bcc: "ali@upappfactory.com, aslam@upappfactory.com, nour@upappfactory.com ,zach@upappfactory.com, saad@upappfactory.com , vaishali.uttam@upappfactory.com",
				html: `<!DOCTYPE html>
					<html lang="en">
		
					<head>
						<meta charset="UTF-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
						<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
						<title>Call Back Request</title>
						<style>
							.bank td,
							th {
								border: 1px solid #dddddd;
								text-align: left;
								padding: 8px;
							}
		
							.bank tr:nth-child(even) {
								background-color: #dddddd;
							}
						</style>
					</head>
		
					<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
						<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
							<tbody>
								<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
									<td style="
									 padding-left: 15px;
								">
										<a href="">
											<img style="width: 150px" width="150"
												src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
										</a>
									</td>
		
								</tr>
								<tr>
									<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
										Hi Sales Team,
										<br>
										We have received a new Call Back request with ticket no: ${contact.UID}, please find the details below:
										<br>
										<br>
										<table class="bank" style="width:100%;border-spacing: 0px;border:none">
		
											<tr>
												<td>Ticket No</td>
												<td>${contact.UID}</td>
											</tr>
											<tr>
												<td>Name</td>
												<td>${contact.name}</td>
											</tr>
											<tr>
												<td>Email Address</td>
												<td>${contact.email}</td>
											</tr>
											<tr>
												<td>Phone No</td>
												<td>${contact.mobile}</td>
											</tr>
											<tr>
												<td>Message</td>
												<td>${contact.description}</td>
											</tr>
										</table>
		
										<br />
										Thanks!
										<br>
										<br />
										Regards,
										<br />
										UPapp factory
									</td>
								</tr>
								<tr style="text-align: center;background: #f7f7f7">
		
									<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
										<p style="margin: 10px auto 0px auto;line-height: 0.8;">
											<a href="mailto:contactus@upappfactory.com" style="text-decoration: none;">contactus@upappfactory.com</a>
											|
											<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
										</p>
										<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25"
												src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
										<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
												src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
										<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
												src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
									</td>
		
								</tr>
								<tr style="padding:20px;text-align: center;background: #f7f7f7">
									<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
										Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
									</td>
								</tr>
							</tbody>
						</table>
					</body>
					</html>`
			}
		}
		await transporter.sendMail(mailOptions)
		await transporter.sendMail(notify)
		res.status(201).send({ error: false, success: true, message: "Contact request success", result: contact })
	} catch (e) {
		console.error(e)
		res.status(500).send({ error: true, success: false, message: "Server Error", result: e })
	}
})

router.post('/request-demo', async (req, res) => {
	try {
		const prevContact = await Contact.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
		let counter, UID, UAF
		if (prevContact.length === 0) {
			counter = 1
		} else {
			counter = prevContact[0].UID.slice(8)
			counter = +counter + 1
		}
		UAF = "UAF-DEM-"
		if (counter < 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 100 && counter >= 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 1000 && counter >= 100) {
			UID = `${UAF}000000${counter}`
		} else if (counter < 10000 && counter >= 1000) {
			UID = `${UAF}00000${counter}`
		} else if (counter < 100000 && counter >= 10000) {
			UID = `${UAF}0000${counter}`
		} else if (counter < 1000000 && counter >= 100000) {
			UID = `${UAF}000${counter}`
		} else if (counter < 10000000 && counter >= 1000000) {
			UID = `${UAF}00${counter}`
		} else if (counter < 100000000 && counter >= 10000000) {
			UID = `${UAF}0${counter}`
		} else {
			UID = `${UAF}${counter}`
		}
		const contact = new Contact(req.body)
		contact.ticket_no = Date.now()
		contact.UID = UID
		const message1 = `\n*****Request For Demo *****\n App interested in : ${contact.interested_in}\n App Name : ${contact.app_name}\n Business Name : ${contact.business_name}\n Business Details: ${contact.description}\n`
		//  name,email,country,mobile,interseted_in,UID 
		// name,source,stage_id,mobile,email,country,sales_rep,lead_score,due_date,next_followup,unsubcribed_at,token,created_at,updated_at,rating_status

		connection.query("INSERT INTO `fx_leads` (`name`,`email`,`country`,`mobile`,`token`,`stage_id`,`sales_rep`,`lead_score`,`due_date`,`next_followup`,`unsubscribed_at`,`created_at`,`updated_at`,`rating_status`,`source`,`message`) VALUES ('" + contact.name + "','" + contact.email + "','" + contact.country + "','" + contact.mobile + "','" + UID + "','42','18','10',DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),'cold','51','" + contact.interested_in + "') ON DUPLICATE KEY UPDATE `message`  = (SELECT CONCAT(message,'" + message1 + "') as msg FROM fx_leads as leads WHERE email='" + contact.email + "')")

		if (!contact)
			return res.status(400).send({ error: true, success: false, message: "Bad Request", result: {} })
		await contact.save()
		let mailOptions
		if (contact.country === "Oman" || contact.country == "Bahrain" || contact.country == "Kuwait" || contact.country == "Qatar" || contact.country == "United Arab Emirates") {
			mailOptions = {
				from: 'zach@upappfactory.com',
				to: contact.email,
				subject: 'Greetings from UPapp!',
				bcc: "sales@upappfactory.com,ali@upappfactory.com,aslam@upappfactory.com,contactus@upappfactory.com",
				html: `<!DOCTYPE html>
                <html lang="en"><head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                    <title>Contact Us</title>
                    <style>
                    .bank td,
                    th {
                    border: 1px solid #dddddd;
                    text-align: left;
                    padding: 8px;
                    }
    
                    .bank tr:nth-child(even) {
                    background-color: #dddddd;
                    }
                    </style>
                    </head>
    
                    <body style="background: #fbfbfb;font-size:14px;font-family: Calibri, sans-serif; line-height: 1.5">
                    <table style="margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
                    <tbody>
                    <tr>
                    <td style="padding: 40px 40px 5px 40px">
                    Hi ${contact.name},
                    <br>
                    <br>
                    This is Zach Naji, Business &amp; Digital Transformation Consultant at UPapp factory. I will be your consultant and contact point at UPapp factory.  <br><br>
    
                    Thank you for visiting UPapp factory’s website and submitting your request to connect. We are glad! <br><br>
    
                    UPapp factory is a Mobile Applications factory with our HQ in Singapore and Oman. We are a team of 80+ Mobile Applications Business Experts. <br><br>
    
                    I understand that you’re looking for a Mobile App probably with a beautiful web portal along with a state-of-the-art system to manage. It would be great if we can set-up a call with you so that we can share ideas and give you some insightful information regarding your requirement.<br><br>
    
                    Appreciate if you can send me an email and suggest a good time to connect. Please add me on WhatsApp or Skype via below mentioned details in the case that's easier. <br><br>
    
                    You can go through our profile with case studies on this <a href="https://go.aws/2Vx9vv0">link</a><br><br>
    
                    Looking forward to hearing from you. 
                    <br>
                    <br>
                    Regards,<br><br>
                    <div style="width:100%;display: -webkit-flex;display:-ms-flexbox;display:flex;Display:flex">
                        <div style="width: 180px;float: left;flex-wrap: wrap;">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo_icon.png" style="
        width: 100%;
    ">
                        </div>
                        <div style="
                    float: left;
                    display: inline-table;
                    flex-wrap: wrap;
                    padding-left: 15px;
                    ">
                            <p style="font-size: 16px;margin:0px">
                                <span style="font-size: 18px;font-weight: 800;color: #4fbfdd;">Zach Naji</span><br><span style="
                    color: #555;
                    ">Business &amp; Digital Transformation Consultant</span><br><span style="
                    font-weight: 700;
                    color: #000;
                    ">UPapp PTE LTD.</span><br>
                               </p><p style="margin:0px"> <a href="https://join.skype.com/invite/majtRFQzZlRf">Skype</a> | <span style="
                                color: #4dc0dc;
                                margin-right: 10px;
                                ">WhatsApp</span> +968 9238 4957</p>
                    <p style="margin:0px"><span style="
                        color: #4dc0dc;
                       
                        font-weight: 800;
                        ">W&nbsp;&nbsp;</span><a href="www.upappfactory.com">www.upappfactory.com</a><span style="
                        color: #4dc0dc;
                       
                        font-weight: 800;
                        ">&nbsp;&nbsp;E&nbsp;&nbsp;</span><a href="mailto:zach@upappfactory.com">zach@upappfactory.com</a></p>
                        <p style="margin:0px">
                        531A, Upper Cross St. #04-95, Hong Lim Complex 051531, Singapore. 
                        </p>
                        <p style="margin:0px"><span style="
                            color: #4dc0dc;
                           
                            font-weight: 800;
                            ">&nbsp;&nbsp;L&nbsp;&nbsp;</span> +65 6714 6696</p>
							<a style="float: left;margin-left: 5px;" href="https://www.linkedin.com/company/upappfactory/"><img style="width:22px" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Email-Signature/linkedIN-circle.png"></a>
							<a style="float: left;margin-left: 5px;" href="https://www.instagram.com/upappfactory/"><img style="width:22px" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Email-Signature/insta-circle-icon.png"></a>
							<a style="float: left;margin-left: 5px;" href="https://www.facebook.com/UPappfactory/"><img style="width:22px" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Email-Signature/fb-icon-circle.png"></a>
                                <p></p>
                    </div>
                    </div>
                    <p></p>
                    </td>
    
                    </tr>
                    </tbody>
                    </table>
                    
            </body></html>`
			}
		} else {
			mailOptions = {
				from: 'harris@upappfactory.com',
				to: contact.email,
				subject: 'Greetings from UPapp!',
				bcc: "sales@upappfactory.com,ali@upappfactory.com,aslam@upappfactory.com,contactus@upappfactory.com,maqsood@upappfactory.com",
				html: `<!DOCTYPE html>
				<html lang="en">
				
				<head>
						<meta charset="UTF-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
						<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
						<title>Contact</title>
						<style>
								.bank td,
								th {
										border: 1px solid #dddddd;
										text-align: left;
										padding: 8px;
								}
				
								.bank tr:nth-child(even) {
										background-color: #dddddd;
								}
						</style>
				</head>
				
				<body style="background: #fbfbfb;font-size:14px;font-family: Calibri, sans-serif; line-height: 1.5">
						<table style="margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
								<tbody>
										<tr>
												<td style="padding: 40px 40px 5px 40px">
												Hi ${contact.name},
														<br>
														<br>
														This is Harris, Business & Digital Transformation Consultant at UPapp factory. I will be your consultant and contact point at UPapp factory.<br><br>

														Thank you for visiting UPapp factory’s website and submitting your request to connect. We are glad! <br><br>

														UPapp factory is a Mobile Applications factory with our HQ in Singapore. We are a team of 80+ Mobile Applications Business Experts.  <br><br>

														I understand that you’re looking for a Mobile App probably with a beautiful web portal along with a state-of-the-art system to manage. It would be great if we can set-up a call with you so that we can share ideas and give you some insightful information regarding your requirement.<br><br>

														Appreciate if you can send me an email and suggest a good time to connect. Please add me on WhatsApp or Skype via below mentioned details in the case that's easier. <br><br>

														You can go through our profile with case studies on this <a href="https://go.aws/2Vx9vv0">link</a><br><br>

														Looking forward to hearing from you. 
														<br>
														<br>
														Regards,<br><br>
                                        <div style="width:100%;display: -webkit-flex;display:-ms-flexbox;display:flex;display: flex;">
										<div style="width: 100px;float: left;flex-wrap: wrap;">
										<img style="width: 100px" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo_icon.png">
									</div>
										<div style="
										float: left;
										color: #615f5f;
										">
                                                <p style="font-size: 16px;margin-top: 0px">
                                                    <span style="font-size: 18px;font-weight: 800;color: #4fbfdd;">Harris A.</span><br><span style="
                color: #555;
            ">Business & Digital Transformation Consultant</span><br><span style="
                font-weight: 700;
                color: #555;
            ">UPapp PTE LTD.</span><br>
                                                    <a href="https://join.skype.com/invite/bmqeN8vvs6hD">Skype</a> | <span style="
            color: #4dc0dc;
   
        ">WhatsApp</span> +968 71547922 (Team) <br><span style="
            color: #4dc0dc;
   
            font-weight: 800;
        ">W &nbsp;&nbsp;</span><a href="www.upappfactory.com">www.upappfactory.com</a><span style="
            color: #4dc0dc;
            font-weight: 800;
        ">&nbsp;&nbsp; E &nbsp;&nbsp;</span><a href="mailto:harris@upappfactory.com">harris@upappfactory.com</a><br>
                                                    531A, Upper Cross St. #04-95, Hong Lim Complex 051531, Singapore. <span style="
            color: #4dc0dc;
          
            font-weight: 800;
        ">&nbsp;&nbsp; L &nbsp;&nbsp;</span> +65 6714 6696<br>
		<a style="float: left;margin-left: 5px;" href="https://www.linkedin.com/company/upappfactory/"><img style="width:22px" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Email-Signature/linkedIN-circle.png"></a>
		<a style="float: left;margin-left: 5px;" href="https://www.instagram.com/upappfactory/"><img style="width:22px" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Email-Signature/insta-circle-icon.png"></a>
		<a style="float: left;margin-left: 5px;" href="https://www.facebook.com/UPappfactory/"><img style="width:22px" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Email-Signature/fb-icon-circle.png"></a>
                                                    </p>
                                            </div>
                                        </div>
                                    </p>
                                </td>

                        </tr>
                </tbody>
        </table>
</body></html>`
			}
		}
		let notify
		notify = {
			from: process.env.EMAIL_USERNAME,
			to: "contactus@upappfactory.com",
			subject: `#${contact.UID} Demo Request for ${contact.name}, country ${contact.country}`,
			bcc: " ali@upappfactory.com, aslam@upappfactory.com, sales@upappfactory.com",
			subject: 'Greeting From UPapp factory!',
			html: `<!DOCTYPE html>
				<html lang="en">
				
				<head>
					<meta charset="UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					<title>Bank Transfer</title>
					<style>
						.bank td,
						th {
							border: 1px solid #dddddd;
							text-align: left;
							padding: 8px;
						}
				
						.bank tr:nth-child(even) {
							background-color: #dddddd;
						}
					</style>
				</head>
				
				<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
					<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
						<tbody>
							<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
								<td style="
								 padding-left: 15px;
							">
									<a href="">
										<img style="width: 150px" width="150"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
									</a>
								</td>
				
							</tr>
							<tr>
								<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
									Hi Sales Team,
									<br>
									We have received a new demo request with ticket no: ${contact.UID}, please find the details below:
									<br>
									<br>
									<table class="bank" style="width:100%;border-spacing: 0px;border:none">
				
										<tr>
											<td>Ticket No</td>
											<td>${contact.UID}</td>
										</tr>
										<tr>
										<td>Name</td>
										<td>${contact.name}</td>
									</tr>
									<tr>
										<td>Email Address</td>
										<td>${contact.email}</td>
									</tr>
									<tr>
										<td>Phone No</td>
										<td>${contact.mobile}</td>
									</tr>
									<tr>
										<td>Country</td>
										<td>${contact.country}</td>
									</tr>
									<tr>
										<td>App Type</td>
										<td>${contact.interested_in}</td>
									</tr>
									<tr>
										<td>App Name</td>
										<td>${contact.app_name}</td>
									</tr>
									<tr>
										<td>Businesss Name</td>
										<td>${contact.business_name}</td>
									</tr>
									<tr>
										<td>Details of business</td>
										<td>${contact.description}</td>
									</tr>
									</table>
				
									<br />
									Thanks!
									<br>
									<br />
									Regards,
									<br />
									UPapp factory
								</td>
							</tr>
							<tr style="text-align: center;background: #f7f7f7">
				
								<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
									<p style="margin: 10px auto 0px auto;line-height: 0.8;">
										<a href="mailto:contactus@upappfactory.com" style="text-decoration: none;">contactus@upappfactory.com</a>
										|
										<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
									</p>
									<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
									<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
									<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
								</td>
				
							</tr>
							<tr style="padding:20px;text-align: center;background: #f7f7f7">
								<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
									Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
								</td>
							</tr>
						</tbody>
					</table>
				</body>
				
				</html>`
		}
		if (contact.country === "Oman" || contact.country == "Bahrain" || contact.country == "Kuwait" || contact.country == "Qatar" || contact.country == "United Arab Emirates") {
			await OmanTransporter.sendMail(mailOptions)
		}
		else {
			await personalTransporter.sendMail(mailOptions)
		}
		await transporter.sendMail(notify)
		res.status(201).send({ error: false, success: true, message: "Contact request success", result: contact })
	} catch (error) {
		console.log(error)
		return res.status(500).send({ error: true, success: false, message: "Server error", result: error })
	}
})

router.get('/contact-us', async (req, res) => {
	try {
		const contacts = await Contact.find({}).sort({ _id: -1 })
		if (!contacts)
			return res.status(404).send({ error: true, success: false, message: "No requests found", result: {} })
		return res.status(200).send({ error: false, success: true, message: "Request found", result: contacts })
	} catch (e) {
		res.status(500).send({ error: true, success: false, message: "server error", result: e })
	}
})

router.post('/make_your_app', async (req, res) => {
	try {

		const prevContact = await Contact.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
		let counter, UID, UAF
		if (prevContact.length === 0) {
			counter = 1
		} else {
			counter = prevContact[0].UID.slice(8)
			counter = +counter + 1
		}
		UAF = "UAF-CUS-"
		if (counter < 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 100 && counter >= 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 1000 && counter >= 100) {
			UID = `${UAF}000000${counter}`
		} else if (counter < 10000 && counter >= 1000) {
			UID = `${UAF}00000${counter}`
		} else if (counter < 100000 && counter >= 10000) {
			UID = `${UAF}0000${counter}`
		} else if (counter < 1000000 && counter >= 100000) {
			UID = `${UAF}000${counter}`
		} else if (counter < 10000000 && counter >= 1000000) {
			UID = `${UAF}00${counter}`
		} else if (counter < 100000000 && counter >= 10000000) {
			UID = `${UAF}0${counter}`
		} else {
			UID = `${UAF}${counter}`
		}
		const make_your_app = new Contact(req.body)
		make_your_app.ticket_no = Date.now()
		make_your_app.UID = UID
		const message1 = `\n *****Customised App*****\n Subject: ${make_your_app.subject}\n  message:${make_your_app.description}\n`

		connection.query("INSERT INTO `fx_leads` (`name`,`email`,`country`,`mobile`,`token`,`stage_id`,`sales_rep`,`lead_score`,`due_date`,`next_followup`,`unsubscribed_at`,`created_at`,`updated_at`,`rating_status`,`source`,`message`) VALUES ('" + make_your_app.name + "','" + make_your_app.email + "','" + make_your_app.country + "','" + make_your_app.mobile + "','" + UID + "','42','18','10',DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),'cold','51','" + message1 + "') ON DUPLICATE KEY UPDATE `message`  = (SELECT CONCAT(message,'" + message1 + "') as msg FROM fx_leads as leads WHERE email='" + make_your_app.email + "')")

		if (!make_your_app)
			return res.status(400).send({ error: true, success: false, message: "Bad Request", result: {} })
		await make_your_app.save()
		const mailOptions = {
			from: process.env.EMAIL_USERNAME,
			to: make_your_app.email,
			subject: `Request for customised app`,
			bcc: "contactus@upappfactory.com, sales@upappfactory.com,joseph@upappfactory.com",
			html: `<!DOCTYPE html>
			<html lang="en">
				<head>
					<meta charset="UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					<title>
						Contact Us
					</title>
				</head>
				
				<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
					<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
						<tbody>
							<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
								<td style="padding-left: 15px;">
									<a href="">
										<img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
									</a>
								</td>
							</tr>
							<td style="font-size:14px; font-family: Helvetica, sans-serif; color: #333333;padding: 20px;">
								Dear ${make_your_app.name},
								<br /><br />
								We understand that you are trying to reach out to the sales team
								at UPappfactory.
								Since we have received your request with the ticket
								#${make_your_app.UID}, our sales team will be in touch with you
								very soon to assist you with your concerns.
								<br /><br />
								Regards,
								<br />
								Support Team
								<br />
								UPappfactory
							</td>
							<tr style="text-align: center;background: #f7f7f7">
								<td style="padding:0px 20px;text-align: center;background: #f7f7f7">
								</td>
							</tr>
							<tr style="text-align: center;background: #f7f7f7">
								<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
									<p style="margin: 10px auto 0px auto;line-height: 0.8;">
										<a href="mailto:contactus@upappfactory.com" style="text-decoration: none;">contactus@upappfactory.com</a>
										|
										<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
									</p>
									<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
									<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
									<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
								</td>
							</tr>
							<tr style="padding:20px;text-align: center;background: #f7f7f7">
								<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
									Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
								</td>
							</tr>
						</tbody>
					</table>
				</body>
			</html>`
		}
		await transporter.sendMail(mailOptions)
		const notify = {
			from: process.env.EMAIL_USERNAME,
			to: "contactus@upappfactory.com",
			subject: `Request for customised app`,
			bcc: "ali@upappfactory.com, aslam@upappfactory.com,joseph@upappfactory.com,zach@upappfactory.com, saad@upappfactory.com, vaishali.uttam@upappfactory.com",
			html: `<!DOCTYPE html>
			<html lang="en">
			
			<head>
				<meta charset="UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
				<title>Call Back Request</title>
				<style>
					.bank td,
					th {
						border: 1px solid #dddddd;
						text-align: left;
						padding: 8px;
					}
			
					.bank tr:nth-child(even) {
						background-color: #dddddd;
					}
				</style>
			</head>
			
			<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
				<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
					<tbody>
						<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
							<td style="
							 padding-left: 15px;
						">
								<a href="">
									<img style="width: 150px" width="150"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
								</a>
							</td>
			
						</tr>
						<tr>
							<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
								Hi Sales Team,
								<br>
								We have received a new request for customised app with ticket no: ${make_your_app.UID}, please find the details below:
								<br>
								<br>
								<table class="bank" style="width:100%;border-spacing: 0px;border:none">
			
									<tr>
										<td>Ticket No</td>
										<td>${make_your_app.UID}</td>
									</tr>
									<tr>
										<td>Name</td>
										<td>${make_your_app.name}</td>
									</tr>
									<tr>
										<td>Email Address</td>
										<td>${make_your_app.email}</td>
									</tr>
									<tr>
										<td>Phone No</td>
										<td>${make_your_app.mobile}</td>
									</tr>
									<tr>
										<td>Subject</td>
										<td>${make_your_app.subject}</td>
									</tr>
									<tr>
										<td>Message</td>
										<td>${make_your_app.description}</td>
									</tr>
								</table>
			
								<br />
								Thanks!
								<br>
								<br />
								Regards,
								<br />
								UPapp factory
							</td>
						</tr>
						<tr style="text-align: center;background: #f7f7f7">
			
							<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
								<p style="margin: 10px auto 0px auto;line-height: 0.8;">
									<a href="mailto:contactus@upappfactory.com" style="text-decoration: none;">contactus@upappfactory.com</a>
									|
									<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
								</p>
								<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
								<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
								<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
							</td>
			
						</tr>
						<tr style="padding:20px;text-align: center;background: #f7f7f7">
							<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
								Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
							</td>
						</tr>
					</tbody>
				</table>
			</body>
			</html>`
		}
		await transporter.sendMail(notify)
		return res.status(201).send({ error: false, success: true, message: "New custom app success", result: make_your_app })
	} catch (error) {
		console.error(error)
		return res.status(500).send({ error: true, success: false, message: "Server error", result: error })
	}
})

router.post('/upappdream', async (req, res) => {
	try {
		const prevContact = await Contact.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
		let counter, UID, UAF
		if (prevContact.length === 0) {
			counter = 1
		} else {
			counter = prevContact[0].UID.slice(8)
			counter = +counter + 1
		}
		UAF = "UAF-DRE-"
		if (counter < 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 100 && counter >= 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 1000 && counter >= 100) {
			UID = `${UAF}000000${counter}`
		} else if (counter < 10000 && counter >= 1000) {
			UID = `${UAF}00000${counter}`
		} else if (counter < 100000 && counter >= 10000) {
			UID = `${UAF}0000${counter}`
		} else if (counter < 1000000 && counter >= 100000) {
			UID = `${UAF}000${counter}`
		} else if (counter < 10000000 && counter >= 1000000) {
			UID = `${UAF}00${counter}`
		} else if (counter < 100000000 && counter >= 10000000) {
			UID = `${UAF}0${counter}`
		} else {
			UID = `${UAF}${counter}`
		}
		const contact = new Contact(req.body)
		contact.mobile = `${req.body.countryCode}${req.body.tel}`
		contact.business_name = req.body.interested
		contact.languages = req.body.language
		contact.budget = req.body.budget
		contact.ticket_no = Date.now()
		contact.type = 'upappdream'
		contact.UID = UID
		console.log(contact, "contact>>>>>>>>>")
		if (!contact)
			return res.status(400).send({ error: true, success: false, message: "Bad Request", result: {} })
		await contact.save()
		const mailOptions = {
			from: 'samual@upappfactory.com',
			to: req.body.email,
			subject: `Turn Your Ideas Into Reality`,
			bcc: "ali@upappfactory.com,aslam@upappfactory.com,samuel@upappfactory.com",
			html: `<!DOCTYPE html>
			<html lang="en">
			
			<head>
					<meta charset="UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					<title>Bank Transfer</title>
					<style>
							.bank td,
							th {
									border: 1px solid #dddddd;
									text-align: left;
									padding: 8px;
							}
			
							.bank tr:nth-child(even) {
									background-color: #dddddd;
							}
					</style>
			</head>
			
			<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
					<table style="margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
							<tbody>
									<tr>
											<td style="padding: 40px 40px 5px 40px">
											<p>
													Good day ${contact.name},<br>
	</p>
													<p>Hope you're doing well.  <br></p>

													<p>Introducing myself as your business & digital transformation consultant at UPapp factory.<br> </p>
													<p>Thank you for expressing your interest in exploring the services & expertise that we've used to work with tens of brands globally. <br></p>
													<p>For us to work with you in transforming your business idea into a Mobile App, can we connect to discuss? You may use the following <a href="https://calendly.com/samuel-78">calendly link</a> to book a time slot at your convenience.<br></p>

													<p>We are excited about your business case and keenly looking forward to working with you.<br></p>

													<p>Thanks for your time.
													<br></p>
													<p>Regards<br></p>
													<p style="font-size: 14px;margin-top: 0px">
													Samual<br>
													Business Consultant<br>
													samual@upappfactory.com <br>
													WhatsApp +968 92804259 (OMN)<br>
													Calendly-<a href="https://calendly.com/samuel-78">https://calendly.com/samuel-78</a><br>
													</p>
											</td>
									</tr>
									<tr>
											<td style="padding: 0px 10px 20px 10px">
													<img width="100%" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/UAF+-+Email+Signature+Finalized-01.png" />
											</td>
									</tr>
							</tbody>
					</table>
			</body></html>`
		}

		const notify = {
			from: process.env.EMAIL_USERNAME,
			to: "samual@upappfactory.com",
			bcc: "ali@upappfactory.com,contactus@upappfactory.com,sales@upappfactory.com,aslam@upappfactory.com,samuel@upappfactory.com",
			subject: `UPapp Dream New Lead Request`,
			html: `<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
				<title>Call Back Request</title>
				<style>
					.bank td,
					th {
						border: 1px solid #dddddd;
						text-align: left;
						padding: 8px;
					}

					.bank tr:nth-child(even) {
						background-color: #dddddd;
					}
				</style>
			</head>

			<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
				<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
					<tbody>
						<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
							<td style="
							 padding-left: 15px;
						">
								<a href="">
									<img style="width: 150px" width="150"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
								</a>
							</td>

						</tr>
						<tr>
							<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
								Hi ,
								<br>
								We have received a new business request with ticket no: ${contact.UID}, please find the details below:
								<br>
								<br>
								<table class="bank" style="width:100%;border-spacing: 0px;border:none">

									<tr>
										<td>Ticket No</td>
										<td>${contact.UID}</td>
									</tr>
									<tr>
										<td>Name</td>
										<td>${contact.name}</td>
									</tr>
									<tr>
										<td>Email Address</td>
										<td>${contact.email}</td>
									</tr>
									<tr>
										<td>Phone No</td>
										<td>${contact.mobile}</td>
									</tr>
									<tr>
										<td>Project title</td>
										<td>${contact.business_name}</td>
									</tr>
									<tr>
										<td>Languages needed in</td>
										<td>${contact.languages}</td>
									</tr>
									<tr>
									<td>Budget</td>
									<td>${contact.budget}</td>
								</tr>
									<tr>
										<td>Description</td>
										<td>${contact.description}</td>
									</tr>
								</table>

								<br />
								Thanks!
								<br>
								<br />
								Regards,
								<br />
								UPapp factory
							</td>
						</tr>
						<tr style="text-align: center;background: #f7f7f7">

							<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
								<p style="margin: 10px auto 0px auto;line-height: 0.8;">
									<a href="mailto:contactus@upappfactory.com" style="text-decoration: none;">contactus@upappfactory.com</a>
									|
									<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
								</p>
								<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
								<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
								<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
							</td>

						</tr>
						<tr style="padding:20px;text-align: center;background: #f7f7f7">
							<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
								Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
							</td>
						</tr>
					</tbody>
				</table>
			</body>
			</html>`
		}
		await BusinessTransporter.sendMail(mailOptions)
		await transporter.sendMail(notify)
		// res.status(201).send({ error: false, success: true, message: "Contact request success", result: contact })
		res.redirect('https://upappfactory.com/upappdream/thank-you.html')
	} catch (error) {
		console.log(error)
		res.status(400).send({ error: true, success: false, message: "Bad Request", result: error })
	}

})

module.exports = router