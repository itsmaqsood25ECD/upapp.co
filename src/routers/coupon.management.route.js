const express = require('express')
const Coupon = require('../models/coupon.management.model')


const router = new express.Router()


router.post('/coupon-management', async (req, res) => {
    const coupon = new Coupon(req.body)
    try {
        await coupon.save()
        console.log(`coupon has been created successfully`)
        res.status(201).send(coupon)
    } catch (e) {
        console.error(e)
        res.status(400).send(e)
    }
})

router.get('/coupon-management', async (req, res) => {
    try {
        const coupons = await Coupon.find({})
        res.send(JSON.stringify(coupons))
    } catch (e) {
        console.error(e)
        res.status(500).send(e)
    }
})

router.get('/coupon-management/:name', async (req, res) => {
    try {
        const coupon = await Coupon.findByName(req.params.name)
        res.send(JSON.stringify(coupon))
    } catch (e) {
        console.error(e)
        res.status(500).send(e)
    }
})

router.delete('/coupon-management/:name', async (req, res) => {
    try {
        const coupon = await Coupon.findByName(req.params.name)
        await coupon.remove()
    } catch (e) {
        console.error(e)
        res.status(500).send(e)
    }
})


router.put('/coupon-management/:name', async (req, res) => {
    try {
        const coupon = await Coupon.findOneAndUpdate({ coupon_code: req.params.name }, { $set: req.body })
        console.log(`coupon with code:${req.params.name} has been updated.`)
        res.send(coupon)
    } catch (e) {
        console.error(e)
        res.status(500).send(e)
    }
})

router.delete('/coupon-management/:id', async (req, res) => {
    try {
        const coupon = await Coupon.findByIdAndDelete(id)
        console.log(`coupon with id:${id} has been deleted`)
        res.send(coupon)
    } catch (e) {
        console.error(e)
        res.status(500).send(e)
    }
})

module.exports = router